//use cohesive-zone-argmin::*;
mod etc_tests {
    use assert_cmd::Command;

    #[test]
    fn unexpected_flag() {
        Command::cargo_bin(env!("CARGO_PKG_NAME"))
            .unwrap()
            .arg("-C")
            .assert()
            .failure();
    }

    #[test]
    fn arg_thrashing() {
        Command::cargo_bin(env!("CARGO_PKG_NAME"))
            .unwrap()
            .arg("-a")
            .arg("newton-cg")
            .arg("-c")
            .args(&["2", "3", "4", "5", "2.8"])
            .arg("--sigma-c")
            .arg("5")
            .arg("--delta-c")
            .arg("5")
            .assert()
            .failure();
    }
}

mod algorithm_flag_tests {
    use assert_cmd::Command;

    #[test]
    fn no_algorithm() {
        Command::cargo_bin(env!("CARGO_PKG_NAME"))
            .unwrap()
            .arg("-a")
            .assert()
            .failure();
    }

    #[test]
    fn wrong_algorithm() {
        Command::cargo_bin(env!("CARGO_PKG_NAME"))
            .unwrap()
            .arg("-a")
            .arg("NEWTON-CG")
            .assert()
            .failure();
    }

    #[test]
    fn newton_algorithm() {
        Command::cargo_bin(env!("CARGO_PKG_NAME"))
            .unwrap()
            .arg("-a")
            .arg("newton")
            .assert()
            .success();
    }

    #[test]
    fn newton_cg_algorithm() {
        Command::cargo_bin(env!("CARGO_PKG_NAME"))
            .unwrap()
            .arg("-a")
            .arg("newton-cg")
            .assert()
            .success();
    }
}

mod const_individual_flag_tests {
    use assert_cmd::Command;

    #[test]
    fn no_flag() {
        Command::cargo_bin(env!("CARGO_PKG_NAME"))
            .unwrap()
            .arg("-a")
            .arg("newton-cg")
            .arg("--sigma-c")
            .assert()
            .failure();
    }

    #[test]
    fn a_flag() {
        Command::cargo_bin(env!("CARGO_PKG_NAME"))
            .unwrap()
            .arg("-a")
            .arg("newton-cg")
            .arg("--sigma-c")
            .arg("5")
            .arg("--delta-c")
            .arg("5")
            .assert()
            .success();
    }

    #[test]
    fn all_flags() {
        Command::cargo_bin(env!("CARGO_PKG_NAME"))
            .unwrap()
            .arg("-a")
            .arg("newton-cg")
            .arg("--sigma-c")
            .arg("5")
            .arg("--delta-c")
            .arg("5")
            .arg("--k")
            .arg("5")
            .arg("--dh")
            .arg("5")
            .arg("--guess")
            .arg("5")
            .assert()
            .success();
    }
}
