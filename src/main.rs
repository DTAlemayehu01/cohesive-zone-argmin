use argmin::{
    core::{observers::ObserverMode, CostFunction, Error, Executor, Gradient, Hessian},
    solver::{
        linesearch::MoreThuenteLineSearch,
        newton::{Newton, NewtonCG},
    },
};
use argmin_observer_paramwriter::{ParamWriter, ParamWriterFormat};
use argmin_observer_slog::SlogLogger;
//use num::{Float, /*FromPrimitive*/};
use clap::{Args, Parser, ValueEnum};
//use ndarray::{array, Array1, Array2}; <- required for bfgs?

#[derive(Args, Debug, Copy, Clone)]
struct CohesiveZone {
    /// Sets sigma constant of CZM
    #[arg(long, value_name = "SIGMA_C", default_value = "3.0")]
    sigma_c: f64,

    /// Sets delta constatnt of CZM
    #[arg(long, value_name = "DELTA_C", default_value = "2.0")]
    delta_c: f64,

    /// Sets k constatnt of Material Response function
    #[arg(long, value_name = "MATERIAL_RESPONSE_K", default_value = "1.0")]
    k: f64,

    /// Sets displacement h constatnt of Material Response function
    #[arg(long, value_name = "DISPLACEMENT_H", default_value = "4.0")]
    dh: f64,

    /// Sets the inital guess of the numerial method used to solve for the model
    #[arg(long, value_name = "INITAL_GUESS", default_value = "2.8")]
    guess: f64,
}

impl CohesiveZone {
    fn cohesive_energy(&self, x: f64) -> f64 {
        let d = self.delta_c;
        -self.sigma_c * (d + x) * (1.0 - x / d).exp()
    }
    fn cohesive_traction(&self, x: f64) -> f64 {
        let d = self.delta_c;
        (self.sigma_c / d) * x * (1.0 - x / d).exp()
    }
    fn d_cohesive_traction(&self, x: f64) -> f64 {
        let d = self.delta_c;
        (self.sigma_c / d) * (1.0 - x / d) * (1.0 - x / d).exp()
    }
    fn elastic_energy(&self, x: f64) -> f64 {
        0.5 * self.k * (x - self.dh).powi(2)
    }
    fn elastic_traction(&self, x: f64) -> f64 {
        self.k * (x - self.dh)
    }
    fn d_elastic_traction(&self, _x: f64) -> f64 {
        self.k
    }
}

impl From<[f64; 5]> for CohesiveZone {
    fn from(p: [f64; 5]) -> Self {
        Self {
            sigma_c: p[0],
            delta_c: p[1],
            k: p[2],
            dh: p[3],
            guess: p[4],
        }
    }
}

impl CostFunction for CohesiveZone {
    type Param = f64;
    type Output = f64;

    fn cost(&self, x: &Self::Param) -> Result<Self::Output, Error> {
        let x = *x;
        Ok(self.cohesive_energy(x) + self.elastic_energy(x))
    }
}

impl Gradient for CohesiveZone {
    type Param = f64;
    type Gradient = f64;

    fn gradient(&self, x: &Self::Param) -> Result<Self::Gradient, Error> {
        let x = *x;
        Ok(self.cohesive_traction(x) + self.elastic_traction(x))
    }
}

impl Hessian for CohesiveZone {
    type Param = f64;
    type Hessian = f64;

    fn hessian(&self, x: &Self::Param) -> Result<Self::Hessian, Error> {
        let x = *x;
        Ok(self.d_cohesive_traction(x) + self.d_elastic_traction(x))
    }
}

// Function to execute the newton solver and log results
fn run_newton(cz: CohesiveZone, observer_mode: ObserverMode) -> Result<(), Error> {
    let init_param: f64 = cz.guess;

    let solver: Newton<f64> = Newton::new();

    let res = Executor::new(cz, solver)
        .configure(|state| state.param(init_param).max_iters(10))
        .add_observer(SlogLogger::term(), observer_mode)
        .add_observer(
            ParamWriter::new("params", "param", ParamWriterFormat::JSON),
            ObserverMode::Every(1),
        )
        .run()?;

    println!("{res}");

    let x = res.state().param;
    if let Some(value) = x {
        println!("THE ANSWER IS: {value:?}");
    }
    Ok(())
}

//function to execute the line search and bgfs solver and log results
fn run_newton_cg(cz: CohesiveZone, observer_mode: ObserverMode) -> Result<(), Error> {
    let init_param: f64 = cz.guess;
    //let init_hessian : f64 = 1.0; <- required for bfgs?

    let linesearch = MoreThuenteLineSearch::new();
    let solver = NewtonCG::new(linesearch);

    let res = Executor::new(cz, solver)
        .configure(|state| state.param(init_param).max_iters(10))
        .add_observer(SlogLogger::term(), observer_mode)
        .add_observer(
            ParamWriter::new("params", "param", ParamWriterFormat::JSON),
            ObserverMode::Every(1),
        )
        .run()?;

    //print result
    println!("{res}");
    Ok(())
}

#[derive(ValueEnum, Debug, Clone)]
enum Algorithm {
    Newton,
    NewtonCG,
}

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct CLI {
    #[command(flatten)]
    cohesive_zone: CohesiveZone,

    /// Chosen method for solving Model
    #[arg(value_enum, short, long)]
    algorithm: Algorithm,

    #[cfg(feature = "plotting")]
    #[arg(long)]
    plot: bool,
}

fn main() {
    //try 3.0, 2.0, 1.0, 4.0!
    let args = CLI::parse();
    println!("Solving: {:?}", &args.cohesive_zone);
    #[cfg(feature = "plotting")]
    if args.plot {
        use rgb::RGB;
        use textplots::{Chart, ColorPlot, Shape};
        let cz = &args.cohesive_zone;
        let colors = (
            RGB::from((0xda, 0x62, 0x7d)),
            RGB::from((0xfc, 0xa1, 0x7d)),
            RGB::from((0x86, 0xbb, 0xd8)),
        );
        println!("Energy");
        let c0 = cz.cohesive_energy(0.0) as f32; // Subtract off this arbitrary constant value
        Chart::new(120, 60, 0., 3.)
            .linecolorplot(
                &Shape::Continuous(Box::new(|x| cz.cohesive_energy(x as f64) as f32 - c0)),
                colors.0,
            )
            .linecolorplot(
                &Shape::Continuous(Box::new(|x| cz.elastic_energy(x as f64) as f32)),
                colors.1,
            )
            .linecolorplot(
                &Shape::Continuous(Box::new(|x| {
                    let x = x as f64;
                    cz.cost(&x).unwrap() as f32 - c0
                })),
                colors.2,
            )
            .display();
        println!("Traction");
        Chart::new(120, 60, 0., 3.)
            .linecolorplot(
                &Shape::Continuous(Box::new(|x| cz.cohesive_traction(x as f64) as f32)),
                colors.0,
            )
            .linecolorplot(
                &Shape::Continuous(Box::new(
                    |x| // Negative elastic traction so it's easy to see the intersection.
                    -cz.elastic_traction(x as f64) as f32,
                )),
                colors.1,
            )
            .linecolorplot(
                &Shape::Continuous(Box::new(|x| {
                    let x = x as f64;
                    cz.gradient(&x).unwrap() as f32
                })),
                colors.2,
            )
            .display();
    }
    match args.algorithm {
        Algorithm::Newton => {
            if let Err(ref e) = run_newton(args.cohesive_zone, ObserverMode::Always) {
                println!("{e}");
                std::process::exit(1);
            }
        }
        Algorithm::NewtonCG => {
            if let Err(ref e) = run_newton_cg(args.cohesive_zone, ObserverMode::Always) {
                println!("{e}");
                std::process::exit(1);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use ndarray_linalg::assert_aclose;
    use rand::random;

    // The model only makes physical sense with nonnegative parameters. The
    // Standard distribution for floating point numbers draws from [0,1).
    //
    // This could go above in the public interface if random cohesive zone
    // models were useful beyond testing.
    impl rand::distributions::Distribution<CohesiveZone> for rand::distributions::Standard {
        fn sample<R: rand::Rng + ?Sized>(&self, rng: &mut R) -> CohesiveZone {
            CohesiveZone {
                sigma_c: rng.gen::<f64>(),
                delta_c: rng.gen::<f64>(),
                k: rng.gen::<f64>(),
                dh: rng.gen::<f64>(),
                guess: rng.gen::<f64>(),
            }
        }
    }

    #[test]
    fn test_newton() {
        for _ in 1..1501 {
            let cz: CohesiveZone = random();
            if let Err(ref e) = run_newton(cz, ObserverMode::Never) {
                println!("{e}");
            };
        }
    }

    #[test]
    fn test_newton_cg() {
        for _ in 1..1501 {
            let cz: CohesiveZone = random();
            if let Err(ref e) = run_newton_cg(cz, ObserverMode::Never) {
                println!("{e}");
            };
        }
    }

    /* A test for if the solvers return similarish answers
    #[test]
    fn test_solver_similarity() {
        let mut bin1 : [f64; 10] = [0.0; 10];
        let mut bin2 : [f64; 10] = [0.0; 10];

        let linesearch = MoreThuenteLineSearch::new();
        let solver1: Newton<f64> = Newton::new();
        let solver2 = NewtonCG::new(linesearch);

        for i in 0..10 {
            let cz1: CohesiveZone = random();
            let cz2: CohesiveZone = cz1.clone();
            let guess1 = cz1.guess;
            let guess2 = cz2.guess;

            let res1 = Executor::new(cz1, solver1)
                .configure(|state| state.param(guess1).max_iters(10))
                .add_observer(SlogLogger::term(), ObserverMode::Never)
                .run();

            //not exactly sure how to handle the case of an error?
            //
            //maybe only let this test run once it's confirmed the
            //function will return values?
            let x = res1.unwrap().state().param;
            match x {
                Some(value) => bin1[i] = value,
                _ => bin1[i] = 0.0,
            }

            let res2 = Executor::new(cz2, solver2.clone())
                .configure(|state| state.param(guess2).max_iters(10))
                .add_observer(SlogLogger::term(), ObserverMode::Never)
                .run();

            let y = res2.unwrap().state().param;
            match y {
                Some(value) => bin2[i] = value,
                _ => bin2[i] = 0.0,
            }
        };
        assert_eq!(bin1, bin2);
    }
    */

    fn finite_difference<F: Fn(f64) -> f64>(f: F, x: f64) -> f64 {
        let eps = 2e-16_f64.cbrt();
        (f(x + eps) - f(x - eps)) / (2. * eps)
    }

    #[test]
    fn cohesive_gradient() {
        let cz: CohesiveZone = random();
        let x = random();
        let fd = finite_difference(|x| cz.cohesive_energy(x), x);
        let trac = cz.cohesive_traction(x);
        assert_aclose!(trac, fd, 1e-8);
    }

    #[test]
    fn cohesive_hessian() {
        let cz: CohesiveZone = random();
        let x = random();
        let fd = finite_difference(|x| cz.cohesive_traction(x), x);
        let d_traction = cz.d_cohesive_traction(x);
        assert_aclose!(fd, d_traction, 1e-8);
    }
    #[test]
    fn elastic_gradient() {
        let cz: CohesiveZone = random();
        let x = random();
        let fd = finite_difference(|x| cz.elastic_energy(x), x);
        let trac = cz.elastic_traction(x);
        assert_aclose!(trac, fd, 1e-8);
    }

    #[test]
    fn elastic_hessian() {
        let cz: CohesiveZone = random();
        let x = random();
        let fd = finite_difference(|x| cz.elastic_traction(x), x);
        let d_traction = cz.d_elastic_traction(x);
        assert_aclose!(fd, d_traction, 1e-8);
    }
}
